import React, { useState, useEffect } from 'react';
import Loader from './../Loader/Loader';
import { cors_api_url } from './../../common/constants.js';


const Elephant = (props) => {
  const { url } = props.match;

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const elephant_url = `https://elephant-api.herokuapp.com/${url}`;

  useEffect(() => {
    setLoading(true);

    fetch(`${cors_api_url}${elephant_url}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error('Error:', error);
        setError(error);
      })
      .finally(() => setLoading(false));
  }, [elephant_url]);


  if (loading) {
    return <Loader />
  }

  if(error){
    return (<h1 className="mt-5">Elephant info not available!</h1>)
  }

  return (
    <>
      <div className="container text-center">
        <div className="row">
          <div
            className="col-12 mt-5 mb-5"
            key={data._id}
          >
            <img
              src={data.image}
              alt="Elephant"
              style={{ width: "500px" }}
              className="img-fluid mt-2 mb-2"
            />
            <h1>Name: {data.name}</h1>
            <h2>Affiliation: {data.affiliation}</h2>
            <h3>Species: {data.species}</h3>
            <p>Sex: {data.sex}</p>
            <p>Fictional: {data.fictional}</p>
            <div>
              <span>{data?.dob} {data?.dod?.length > 1 ? ('- ' + data.dod) : " "}</span>
            </div>
            <div>
              <a href={data.wikilink}>Wikipedia</a>
            </div>
            <p>{data.note}</p>
          </div>
        </div>
      </div>

    </>
  )
}


export default Elephant;