import React, { useState, useEffect } from 'react';
import ElephantsView from './ElephantsView.js';
import Loader from './../Loader/Loader';
import { cors_api_url } from './../../common/constants.js';

const Elephants = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const elephants_url = 'https://elephant-api.herokuapp.com/elephants';
  useEffect(() => {
    setLoading(true);

    fetch(`${cors_api_url}${elephants_url}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.error('Error:', error);
      })
      .finally(() => setLoading(false));
  }, []);


  if (loading) {
    return <Loader />
  }

  const loadedElephants = data.map(elephant => {
    return (
      <div
        className="col-4 mt-2 mb-2"
        key={elephant._id}
        style={{ cursor: "pointer" }}
        onClick={() => {
          props.history.push(`elephants/name/${elephant?.name}`)
        }}
      >
        {
          (elephant.name && elephant.image) && <ElephantsView {...elephant} />
        }
      </div >
    )
  })

  return (
    <>
      <div className="container">
        <div className="row">
          {loadedElephants}
        </div>
      </div>

    </>
  )
}


export default Elephants;