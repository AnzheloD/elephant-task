import React from 'react';

const ElephantsView = (props) => {

  return (

    <div className="card h-100" >
      <div className="infoContainer" >
        <h4>{props.name}</h4>
        <h5>{props.sex}</h5>
      </div>
      <div className="pt-2 pb-3">
        <img
          src={props.image}
          alt="Elephant"
          style={{ width: "200px" }}
        />
      </div>
    </div >
  )
}
export default ElephantsView;

