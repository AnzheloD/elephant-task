import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Elephants from './components/Elephants/Elephants.js';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Elephant from './components/Elephant/Elephant.js';

const App = () => {
  return (
    <BrowserRouter>

      <div className="App">

        <Switch>
          <Route path="/" exact>
            <Redirect to="/elephants" />
          </Route>
          <Route path="/elephants" exact component={Elephants} />
          <Route path="/elephants/name/:name" exact component={Elephant} />

        </Switch>

      </div>

    </BrowserRouter>
  );
}

export default App;
